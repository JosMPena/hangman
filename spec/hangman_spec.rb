require 'spec_helper'
require 'hangman'

describe Hangman do
  let(:hangman) { Hangman.new }

  it 'should select a word between 5 and 12 chars long' do
    expect(5..12).must_include hangman.word.length
  end

  it 'should display how many guesses the player has left' do
    expect(hangman.guesses_left).must_be_kind_of Integer
  end

  it 'should display correct guessed letters' do
    hangman.word = 'guess'
    hangman.guess('s')
    expect(hangman.correct_guesses).must_be_kind_of Array
    expect(hangman.correct_guesses).must_include 's'
  end

  it 'should display incorrect guesses' do
    hangman.word = 'guess'
    hangman.guess('z')
    expect(hangman.incorrect_guesses).must_include 'z'
    expect(hangman.correct_guesses).wont_include 'z'
  end

  it 'should allow the player to make a guess of one letter' do
    hangman.guess('a')
    expect(hangman.guesses).must_include 'a'
    hangman.guess('bc')
    expect(hangman.guesses).wont_include 'b'
  end

  it 'should allow case insensitivity on guess' do
    hangman.word = 'oranges'
    hangman.guess('N')
    expect(hangman.correct_guesses).must_include 'n'
    expect(hangman.incorrect_guesses).wont_include 'N'
  end

  it 'should provide correctness feedback' do
    hangman.word = 'oranges'
    hangman.guess('N')
    expect(hangman.feedback).must_include 'correct'
    hangman.guess('z')
    expect(hangman.feedback).must_include 'incorrect'
  end

  it 'should end the game (losing) if out of guesses' do
    hangman.word = 'oranges'
    hangman.guesses = %w[z z z z z z]
    hangman.guess 'h'
    expect(hangman.feedback).must_include 'lose'
  end

  it 'should serialize and save the game to a file' do
    hangman.save_to_file('my_file')
    expect(File.exist?('./data/my_file.dat')).must_equal true
  end

  it 'should recover saved game\' state' do
    hangman.guess('J')
    hangman.save_to_file('my_file')
    hangman2 = Hangman.new
    hangman2.load_from_file('my_file')
    expect(hangman2.guesses).must_include 'j'
  end
end
