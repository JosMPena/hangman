#
class Game
  def self.play!
    puts 'HANGMAN'
    hangman = Hangman.new
    length = ''
    hangman.word.each_char { length << '_' }
    puts "word: #{length}"
    while hangman.guesses_left?
      puts 'Try a letter'
      puts hangman.guess(gets.chomp)
      puts "Status: #{hangman.word_status}"
    end
  end
end

class Hangman
  attr_accessor :word, :guesses, :correct_guesses, :incorrect_guesses

  def initialize
    @word = ''
    words = File.readlines('./data/5desk.txt')
    @word = words.sample.chomp while word_out_of_length?
    @guesses = []
    @correct_guesses = []
    @incorrect_guesses = []
  end

  def word_out_of_length?
    @word.length < 5 || @word.length > 12
  end

  def guesses_left
    @word.size - @guesses.size
  end

  def guesses_left?
    guesses_left > 0
  end

  def guess(char)
    @guesses << char.downcase
    try = @guesses.last
    return feedback(false) if try.size != 1
    @word.include?(try) ? @correct_guesses << try : @incorrect_guesses << try
    feedback
  end

  def word_status
    status_word = ''
    @word.each_char do |char|
      @correct_guesses.include?(char) ? status_word << char : status_word << '_'
    end
    status_word
  end

  def feedback(result = true)
    try = @guesses.last
    return 'There was an error' unless result
    return 'The guess is incorrect' if @incorrect_guesses.include?(try) && guesses_left?
    return 'The guess is correct' if @correct_guesses.include?(try) && guesses_left?
    return "Game Over, you lose. word was #{@word}" if !guesses_left? && @correct_guesses.join != @word
    'Game Over, you win'
  end

  def save_to_file(filename = 'hangman')
    File.open("./data/#{filename}.dat", 'w') { |file| file.print Marshal::dump(self) }
  end

  def load_from_file(filename)
    loaded = nil
    File.open("./data/#{filename}.dat", 'r') { |object| loaded = Marshal::load(object) }
    loaded.instance_variables.each do |var_name|
      attribute = var_name.to_s.delete('@')
      self.send("#{attribute}=", loaded.send(attribute))
    end
  end
end
